package main

import (
	cm "echo-rest-sqlx/common"
	"echo-rest-sqlx/db"
	"echo-rest-sqlx/routes"
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	//Load Config
	cm.LoadConfig()

	if err := db.InitDatabase(); err != nil {
		log.Error("Unable to connect to database server ", err)
		os.Exit(1)
	}
	
	// Route => handler
	e := routes.Init()
	
	// Start server
	e.Logger.Fatal(e.Start(":3030"))
}