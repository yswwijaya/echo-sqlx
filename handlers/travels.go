package handlers

import (
	"echo-rest-sqlx/common"
	"echo-rest-sqlx/db"
	sr "echo-rest-sqlx/services"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
)

var travel common.TravelList
var travelObj []common.TravelList
type M map[string]interface{}

func FetchTravels(c echo.Context) (res common.Response, err error) {

	req := new(common.Request)

	if err = c.Bind(req); err != nil {
		return res, err
	}

	baseURL := req.Url
	method := "POST"
	data := M{"provinsi": req.Province}
	
	responseBody, err := sr.DoRequest(baseURL, method, data)
	
	if err != nil {
		log.Println("ERROR", err.Error())
		return
	}
	
	if err = json.Unmarshal(responseBody, &travel); err != nil {
		 log.Fatalln(err)
	}

	
	tx := db.DB.MustBegin()
	for _, rec := range travel.Data {
		tx.MustExec("INSERT INTO travels (Trip, Description, TravelID, TravelName, CityName) VALUES (?,?,?,?,?)", rec.TripID, rec.Description, rec.TravelID, rec.TravelName, rec.CityName)
	}
	tx.Commit()

    res.Status = http.StatusOK
	res.Message = "succses"
	res.Data = travel

	return
}