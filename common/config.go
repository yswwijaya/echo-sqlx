package common

import (
	"os"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

//Configuration ...
type Configuration struct {
	DatabaseHost 	  string `required:"true" split_words:"true"`
	DatabasePassword  string `required:"true" split_words:"true"`
	DatabasePort	  string `required:"true" split_words:"true"`
	DatabaseUsername  string `required:"true" split_words:"true"`
	DatabaseName  	  string `required:"true" split_words:"true"`
}


var Config Configuration

func LoadConfig()  {

	if err := envconfig.Process("REST", &Config); err != nil {
		log.Infof("Loaded configs: %+v", Config)
		log.Error(err)
		os.Exit(1)
	}

	log.Infof("Loaded configs: %+v", Config)

}