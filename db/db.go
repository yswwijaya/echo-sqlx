package db

import (
	cm "echo-rest-sqlx/common"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var DB *sqlx.DB

func InitDatabase() (err error) {

	//Create Connection string "<user>:<password>@tcp(<host>:<port>)/<database name>?multiStatements=true&sql_mode=TRADITIONAL&timeout=5s"
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?multiStatements=true&sql_mode=TRADITIONAL&timeout=5s", cm.Config.DatabaseUsername,cm.Config.DatabasePassword, cm.Config.DatabaseHost, cm.Config.DatabasePort, cm.Config.DatabaseName )
	//Connection
	DB, err = sqlx.Connect("mysql", connectionString)
	return
}